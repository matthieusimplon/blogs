package com.example.projectblog.blogs;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;


@SpringBootTest
@AutoConfigureMockMvc
@Sql("/Createtable.sql")

public class CategorieControllerTest {

    @Autowired
    MockMvc mvc;

    @Test
    void testFindAll() throws Exception {

        mvc.perform(get("/api/categorie"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].id").exists())
                .andExpect(jsonPath("$[0].name").value("sport"))
                .andExpect(jsonPath("$[0].description").value("description1"));

    }

    @Test
    void findByIdSuccess() throws Exception {
        mvc.perform(get("/api/categorie/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("sport"))
                .andExpect(jsonPath("$.description").value("description1"));

    }

    @Test
    void findByIdNotFound() throws Exception {
        mvc.perform(get("/api/categorie/1000"))
                .andExpect(status().isNotFound());
    }

    @Test

    void testPostCategorie() throws Exception {
        mvc.perform(post("/api/categorie/")
        .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"name\": \"NouvelleCatégorie\" }"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.name").value("NouvelleCatégorie"))
                .andExpect(jsonPath("$.description").value("Nouvelledescription"));

    }

    @SuppressWarnings("null")
    @Test
   
    void testPostCategorieInvalid() throws Exception {
        mvc.perform(post("/api/categorie")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ }"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testDeleteCategorie() throws Exception {
        mvc.perform(delete("/api/categorie/4"))
                .andExpect(status().isNoContent());
    }

    @SuppressWarnings("null")
    @Test

    void testPutCategorie() throws Exception {
        mvc.perform(put("/api/categorie/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"name\": \"Sports Modifié\" }"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("Sports Modifié"));
    }

    


}
