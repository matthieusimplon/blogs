package com.example.projectblog.blogs;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@Sql("/Createtable.sql")

public class CommantaireControllerTest {

    @Autowired
    MockMvc mvc;

    @Test
    void testFindCommentairesByArticleId() throws Exception {

        try {
            mvc.perform(get("/api/commantaire/{articleId}"))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$[*].id").exists())
                    .andExpect(jsonPath("$[0].contenu").value("contenu1"))
                    .andExpect(jsonPath("$[0].nonAuteur").value("matthieu"))
                    .andExpect(jsonPath("$[0].articleId").value("1"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @SuppressWarnings("null")
    @Test
    void testPersistCommentaire() throws Exception {
        mvc.perform(post("/api/commantaire")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                        {
                            "contenu": "contenu1",
                            "nonAuteur": "matthieu",
                            "dateCommantaire": "2024-03-25",
                            "article_id": 1
                        }
                        """))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$['id']").isNumber());
    }

    @Test
    void testDeleteCommentaire() throws Exception {
        mvc.perform(delete("/api/commantaire/{id}", 3))
                .andExpect(status().isNoContent());
    }

    @Test
    void testReplaceCommentaire() throws Exception {
        mvc.perform(put("/api/commantaire/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                        {
                            "id": 15,
                            "contenu": "Nouveau contenu du commantaire",
                            "nonAuteur": "Nouvel auteur du commantaire",
                            "dateComantaire": "2024-03-29T10:00:00",
                            "article_id": 15
                        }
                        """))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$['id']").value(1))
                        .andExpect(jsonPath("$['contenu']").value("Nouveau contenu du commantaire"))
                        .andExpect(jsonPath("$['nonAuteur']").value("Nouvel auteur du commantaire"))
                        //.andExpect(jsonPath("$['dateCommantaire']").value("1655164800000"))
                        .andExpect(jsonPath("$['article_id").value(1))
                        .andExpect(jsonPath("$['dateCommantaire").value(1655164800000L));
            }
        


}
