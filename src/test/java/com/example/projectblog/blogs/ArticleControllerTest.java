package com.example.projectblog.blogs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;

//Imports nécessaire pour les tests d'API
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Test;





@SuppressWarnings("unused")
@SpringBootTest
@AutoConfigureMockMvc
@Sql("/Createtable.sql")
public class ArticleControllerTest {

    @Autowired
    MockMvc mvc;

   
    @Test
    void testGetAll() throws Exception {
         mvc.perform(get("/api/article"))
        .andExpect(jsonPath("$[*]['id']").exists())
        .andExpect(jsonPath("$[0]['name']").value("name1"))
        .andExpect(jsonPath("$[0]['contenu']").value("contenu1"))
        .andExpect(jsonPath("$[0]['datePublication']").value("2024-03-28"));
    }
    @Test
    void findByIdSuccess() throws Exception {
        mvc.perform(get("/api/article/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("name1"))
                .andExpect(jsonPath("$.contenu").value("contenu1"))
                .andExpect(jsonPath("$.datePublication").value("2024-03-28"));


}

}


