package com.example.ptojetblog.blogs.ripository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.ptojetblog.blogs.Entity.Article;
import com.example.ptojetblog.blogs.Entity.Commentaire;



@Repository

public class CommentaireRepository {
    @Autowired
    private DataSource dataSource;

    public List<Commentaire> findAll() {
        List<Commentaire> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM commantaire");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {

                list.add(new Commentaire(
                        result.getInt("id"),
                        result.getString("contenu"),
                        result.getString("nonAuteur"),
                        result.getDate("dateCommantaire").toLocalDate(),
                        result.getInt("article_id")

                ));

            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;
    }

    public Commentaire findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM commantaire where id = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Commentaire(

                        result.getInt("id"),
                        result.getString("contenu"),
                        result.getString("nonAuteur"),
                        result.getDate("dateCommantaire").toLocalDate(),
                        result.getInt("article_id"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM commantaire where id =?");
            stmt.setInt(1, id);
            int result = stmt.executeUpdate();
            if (result > 0) {
                return true;
            }

        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    
public boolean persist(Commentaire commantaire) {
    try (Connection connection = dataSource.getConnection()) {
        PreparedStatement stmt = connection.prepareStatement( " INSERT INTO commantaire SET  contenu = ?, nonAuteur = ?, dateCommantaire = ? , article_id =?");
       stmt.setString(1, commantaire.getContenu());
       stmt.setString(2, commantaire.getNonAuteur());
       stmt.setDate(3, Date.valueOf(commantaire.getDateCommantaire()));
       stmt.setInt(4, commantaire.getArticle_id());

    

        if (stmt.executeUpdate() == 1) {
            return true;
        }
    } catch (SQLException e) {
        e.printStackTrace();
    }

    return false;
}
 public boolean update(Commentaire commantaire) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(" UPDATE commantaire SET  contenu = ?, nonAuteur = ?, dateCommantaire = ?, article_id =? WHERE id = ?");
                    stmt.setString(1, commantaire.getContenu());
                    stmt.setString(2, commantaire.getNonAuteur());
                    stmt.setDate(3, Date.valueOf(commantaire.getDateCommantaire()));
                    stmt.setInt(4, commantaire.getArticle_id());
                    stmt.setInt(5, commantaire.getId());
    
            if (stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    
        return false;
    }


    public List<Commentaire> findCommantaireByArticleId(int articleId) {
        List<Commentaire> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                "SELECT commantaire.id, commantaire.article_id,  commantaire.contenu,  commantaire.nonAuteur , commantaire.dateCommantaire " +
                "FROM commantaire  " +
                "INNER JOIN article  ON commantaire.article_id = article.id " +
                "WHERE commantaire.article_id = ?");
            stmt.setInt(1, articleId);
            ResultSet result = stmt.executeQuery();
    
            while (result.next()) {
                Article article = new Article();
                article.setId(result.getInt("article_id"));
                //article.setName(null);(result.getString("article_titre"));
    
                Commentaire commantaire = new Commentaire(
                    result.getInt("id"),
                    result.getString("contenu"),
                    result.getString("nonAuteur"),
                    result.getDate("dateCommantaire").toLocalDate(),
                    result.getInt("article_id"));
    
                list.add(commantaire);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
    

}
