package com.example.ptojetblog.blogs.ripository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.ptojetblog.blogs.Entity.Article;
import com.example.ptojetblog.blogs.Entity.Commentaire;

@Repository
public class ArticleRipository {
    
    @Autowired
    private DataSource dataSource;

    public List<Article> findAll() {
        List<Article> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {

                list.add(new Article(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getString("contenu"),
                        result.getDate("datePublication").toLocalDate(),
                        result.getString("auteur"),
                        result.getString("image")));
                     

            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;
    }

    public Article findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article where id = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Article(

                        result.getInt("id"),
                        result.getString("name"),
                        result.getString("contenu"),
                        result.getDate("datePublication").toLocalDate(),
                        result.getString("auteur"),
                        result.getString("image")
                    

                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM article where id =?");
            stmt.setInt(1, id);
            int result = stmt.executeUpdate();
            if (result > 0) {
                return true;
            }

        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    public boolean persist(Article article) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO article (name,contenu,datePublication,auteur,image) VALUES (?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, article.getName());
            stmt.setString(2, article.getContenu());
            stmt.setDate(3, Date.valueOf(article.getDatePublication()));
            stmt.setString(4, article.getAuteur());
            stmt.setString(5, article.getImage());
          

       

           
            if(stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                article.setId(keys.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }

   
    public boolean update(Article article) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    " UPDATE article SET titre = ?, contenu = ?, auteur = ?, date = ?, image = ?,  categorie_id = ? , theme =? WHERE id = ?");
          
            stmt.setString(1, article.getName());
            stmt.setString(2, article.getContenu());
            stmt.setDate(3, Date.valueOf(article.getDatePublication()));
            stmt.setString(4, article.getAuteur());
            stmt.setString(5, article.getImage());
          
            if (stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public List<Commentaire> findByArticleId(int articleId) {
        List<Commentaire> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "SELECT commantaire.id, commantaire.article_id,  contenu,  nonAuteur , dateCommantaire " +
                            "FROM commantaire  " +
                            "INNER JOIN article a ON commantaire.article_id = article.id " +
                            "WHERE commantaire.article_id = ?");
            stmt.setInt(1, articleId);
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Article article = new Article();
                article.setId(result.getInt("article_id"));
                // article.setName(null);(result.getString("article_titre"));

                Commentaire commantaire = new Commentaire(
                        result.getInt("commentaire_id"),
                        result.getString("contenu"),
                        result.getString("nonAuteur"),
                        result.getDate("dateCommantaire").toLocalDate(),
                        result.getInt("article_id"));

                list.add(commantaire);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Article> findAlllastArticles() {
        List<Article> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM article ORDER BY datePublication DESC LIMIT 4");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Article article = new Article(

                        result.getInt("id"),
                        result.getString("name"),
                        result.getString("contenu"),
                        result.getDate("datePublication").toLocalDate(),
                        result.getString("auteur"),
                        result.getString("image"));                       
                list.add(article);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;
    }

    public List<Article> findAllByCategorie(int id) {
        List<Article> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article  where id_categorie = ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            while (result.next()) {

                list.add(new Article(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getString("contenu"),
                        result.getDate("datePublication").toLocalDate(),
                        result.getString("auteur"),
                        result.getString("image")));
                

            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;
    }
}



