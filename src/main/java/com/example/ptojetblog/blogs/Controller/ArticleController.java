package com.example.ptojetblog.blogs.Controller;


import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import com.example.ptojetblog.blogs.Entity.Article;
import com.example.ptojetblog.blogs.ripository.ArticleRipository;

import jakarta.validation.Valid;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;




@RestController
@RequestMapping("/api/article")

public class ArticleController {

    @Autowired
    private ArticleRipository articleRepo;

    @GetMapping
    public List<Article> all() {
        return articleRepo.findAll();
    }

    @GetMapping("/{id}")
    public Article one(@PathVariable int id) {
        Article article = articleRepo.findById(id);
        if (article == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return article;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Article add(@Valid @RequestBody Article article) {
        article.setDatePublication(LocalDate.now());
        articleRepo.persist(article);
        return article;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        one(id); // On relance le one pour renvoyer un 404 si on trouve pas le chien
        articleRepo.delete(id);
    }

    @PutMapping("/{id}")
    public Article replace(@PathVariable int id, @Valid @RequestBody Article article) {
        one(id); // pareil
        article.setId(id); // On met l'id qui a été donné dans l'url au cas où elle ne correspondraient pas
        articleRepo.update(article);
        return article;
    }

    @GetMapping("/last")
    public List<Article> findAlllastArticles() {
        return articleRepo.findAlllastArticles();
    }

    @GetMapping("/categorie/{id}")
    public List<Article> findAllByCategorie(@PathVariable int id) {
        if (articleRepo == null) {

        }
        return articleRepo.findAllByCategorie(id);
    }

    
    @PostMapping("/upload")
    public String upload(@RequestParam MultipartFile image) {
        String renamed = UUID.randomUUID() + ".jpg";
        try {
            Thumbnails.of(image.getInputStream())
                    .width(900)
                    .toFile(new File(getUploadFolder(), renamed));
            Thumbnails.of(image.getInputStream())
                    .size(200, 200)
                    .crop(Positions.CENTER)
                    .toFile(new File(getUploadFolder(), "thumbnail-" + renamed));
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Upload error", e);
        }
        return renamed;
    }

    /**
     * Méthode qui récupère le lien vers le dossier d'uploads et le crée s'il
     * n'existe pas.
     * Il est ici configuré pour le mettre dans le dossier resources/static qui
     * existe et est
     * servi automatiquement par Spring Boot
     * 
     * @return
     */
    private File getUploadFolder() {
        File folder = new File(getClass().getClassLoader().getResource(".").getPath().concat("static/uploads"));
        if (!folder.exists()) {
            folder.mkdirs();
        }
        return folder;

    }

}
