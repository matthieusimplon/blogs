package com.example.ptojetblog.blogs.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.example.ptojetblog.blogs.Entity.Categorie;
import com.example.ptojetblog.blogs.ripository.CategorieRipository;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/categorie")


public class CategorieController {
       
    @Autowired
    private CategorieRipository categorieRepo;

    @GetMapping
    public List<Categorie> all() {
        return categorieRepo.findAll();
    }

    @GetMapping("/{id}")
    public Categorie one(@PathVariable int id) {
        Categorie Categorie = categorieRepo.findById(id);
        if(Categorie == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return Categorie;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Categorie add( @Valid @RequestBody Categorie categorie) {
        categorieRepo.persist(categorie);
        return categorie;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        one(id); //On relance le one pour renvoyer un 404 si on trouve pas le chien
        categorieRepo.delete(id);
    }

    @PutMapping("/{id}")
    public Categorie replace(@PathVariable int id, @Valid @RequestBody Categorie categorie) {
        one(id); //pareil
        categorie.setId(id); //On met l'id qui a été donné dans l'url au cas où elle ne correspondraient pas
        categorieRepo.update(categorie);
        return categorie;
    }

    
}
