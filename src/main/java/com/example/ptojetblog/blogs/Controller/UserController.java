package com.example.ptojetblog.blogs.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.example.ptojetblog.blogs.Entity.User;
import com.example.ptojetblog.blogs.ripository.UserRipository;

import jakarta.validation.Valid;

@RestController
@CrossOrigin

public class UserController {

    @Autowired
    private PasswordEncoder hasher;
    @Autowired
    private UserRipository repo;

    @PostMapping("/api/user")
    public User register(@Valid @RequestBody User user) {

        if(repo.findByEmail(user.getEmail()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already exists");
        }

      
        String hash = hasher.encode(user.getPassword());
        user.setPassword(hash);
        user.setRole("ROLE_USER");
        repo.persist(user);
        return user;

    }


    @CrossOrigin(origins = "http://localhost:5174")
    @GetMapping("/api/account")
    public User myAccount(@AuthenticationPrincipal User user) {
        return user;
    }


    
}
