package com.example.ptojetblog.blogs.Entity;
import java.time.LocalDate;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;


public class Article {
     private Integer id;
    @NotBlank
    private String name;
    @NotBlank
    private String contenu;
    
    private LocalDate datePublication;
    @NotNull
    private String auteur;
    private String image;

  
    
    
    
   

    public Article(String name, String contenu, LocalDate datePublication, String auteur, String image) {
        this.name = name;
        this.contenu = contenu;
        this.datePublication = datePublication;
        this.auteur = auteur;
        this.image = image;
      
    }

    public Article() {
    }
        
    
    public Article(Integer id, @NotBlank String name, @NotBlank String contenu, LocalDate datePublication,
            @NotNull String auteur, String image) {
        this.id = id;
        this.name = name;
        this.contenu = contenu;
        this.datePublication = datePublication;
        this.auteur = auteur;
        this.image = image;
    
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }





    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getContenu() {
        return contenu;
    }


    public void setContenu(String contenu) {
        this.contenu = contenu;
    }


    public LocalDate getDatePublication() {
        return datePublication;
    }


    public void setDatePublication(LocalDate datePublication) {
        this.datePublication = datePublication;
    }


    public String getAuteur() {
        return auteur;
    }


    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }


    public String getImageLink() {
        if(image.startsWith("http")) {
            return image;
        }
        final String baseUrl = 
ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
        return baseUrl+"/uploads/"+image;
    }

    public String getThumbnailLink() {
        if(image.startsWith("http")) {
            return image;
        }
        final String baseUrl = 
ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
        return baseUrl+"/uploads/thumbnail-"+image;
    }
    


}


