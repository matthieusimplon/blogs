package com.example.ptojetblog.blogs.Entity;
import java.time.LocalDate;
public class Commentaire {
    private int id;
    private String contenu;
    private String nonAuteur;
    private LocalDate dateCommantaire;
    private int article_id;
    
    public Commentaire() {
    }

    public Commentaire(String contenu, String nonAuteur, LocalDate dateCommantaire, int article_id) {
        this.contenu = contenu;
        this.nonAuteur = nonAuteur;
        this.dateCommantaire = dateCommantaire;
        this.article_id = article_id;
    }

    public Commentaire(int id, String contenu, String nonAuteur, LocalDate dateCommantaire, int article_id) {
        this.id = id;
        this.contenu = contenu;
        this.nonAuteur = nonAuteur;
        this.dateCommantaire = dateCommantaire;
        this.article_id = article_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getNonAuteur() {
        return nonAuteur;
    }

    public void setNonAuteur(String nonAuteur) {
        this.nonAuteur = nonAuteur;
    }

    public LocalDate getDateCommantaire() {
        return dateCommantaire;
    }

    public void setDateCommantaire(LocalDate dateCommantaire) {
        this.dateCommantaire = dateCommantaire;
    }

    public int getArticle_id() {
        return article_id;
    }

    public void setArticle_id(int article_id) {
        this.article_id = article_id;
    }



    
}
