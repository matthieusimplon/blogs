
package com.example.projectblog.blogs.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.example.projectblog.blogs.entity.User;
import com.example.projectblog.blogs.repository.UserRepository;

import jakarta.validation.Valid;

@RestController
@CrossOrigin
public class AuthController {

    @Autowired
    private PasswordEncoder hasher;
    @Autowired
    private UserRepository repo;

    @PostMapping("/api/user")
    public User register(@Valid @RequestBody User user) {

        if (repo.findByEmail(user.getEmail()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already exists");
        }

        String hash = hasher.encode(user.getPassword());
        user.getEmail();
        user.setPassword(hash);
        user.setRole("ROLE_USER");
        repo.persist(user);
        return user;

    }

    @CrossOrigin(origins = "http://localhost:5173")
    @GetMapping("/api/account")
    public User myAccount(@AuthenticationPrincipal User user) {
        return user;
    }

    @GetMapping("/api/account/{id}")
    public Optional<User> getUserByid(@PathVariable int id) {
        if (repo.findById(id) == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return repo.findById(id);
    }

}
