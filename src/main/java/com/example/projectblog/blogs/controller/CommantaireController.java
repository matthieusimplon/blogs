package com.example.projectblog.blogs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.example.projectblog.blogs.entity.Commantaire;
import com.example.projectblog.blogs.repository.CommantaireRipository;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/commantaire")

public class CommantaireController {
  
    @Autowired
    private CommantaireRipository commantaireRepo;

    @GetMapping
    public List<Commantaire> all() {
        return commantaireRepo.findAll();
    }

    @GetMapping("/{id}")
    public Commantaire one(@PathVariable int id) {
        Commantaire commantaire = commantaireRepo.findById(id);
        if(commantaire == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return commantaire;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Commantaire add( @Valid @RequestBody Commantaire commantaire) {
     commantaireRepo.persist(commantaire);
        return commantaire;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        one(id); //On relance le one pour renvoyer un 404 si on trouve pas le chien
    commantaireRepo.delete(id);
    }

    @PutMapping("/{id}")
    public Commantaire replace(@PathVariable int id, @Valid @RequestBody Commantaire commantaire) {
        one(id); //pareil
        commantaire.setId(id); //On met l'id qui a été donné dans l'url au cas où elle ne correspondraient pas
        commantaireRepo.update(commantaire);
        return commantaire;
    }
    @GetMapping("/article/{articleId}")
    public List<Commantaire> findByArticleId(@PathVariable int articleId) {
        return commantaireRepo.findCommantaireByArticleId(articleId);
    }


}


 
    

