package com.example.projectblog.blogs.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.projectblog.blogs.entity.Categorie;

@Repository

public class CategorieRipository {
    
    
    @Autowired
    private DataSource dataSource;

    public List<Categorie> findAll() {
        List<Categorie> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM categorie");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
    
        
                list.add(new Categorie(
                    result.getInt("id"),
                    result.getString("nom"),
                    result.getString("description")));

            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;

}
public Categorie findById(int id) {
    try (Connection connection = dataSource.getConnection()) {
        PreparedStatement stmt = connection.prepareStatement("SELECT * FROM categorie where id = ?");
        stmt.setInt(1, id);
        ResultSet result = stmt.executeQuery();
        if (result.next()) {
            return new Categorie(
                
                result.getInt("id"),
                result.getString("nom"),
                result.getString("description"));
}
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return null;
}
public boolean delete(int id) {
    try (Connection connection = dataSource.getConnection()) {
        PreparedStatement stmt = connection.prepareStatement("DELETE FROM categorie WHERE id = ?");
        stmt.setInt(1, id);
        int result = stmt.executeUpdate();
        if (result > 0) {
            return true;
        }
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return false;
}

public boolean persist(Categorie categorie) {
    try (Connection connection = dataSource.getConnection()) {
        PreparedStatement stmt = connection.prepareStatement( " INSERT INTO categorie SET id = ?, nom = ?, description = ?");
       stmt.setInt(1, categorie.getId());
       stmt.setString(2, categorie.getName());
       stmt.setString(3, categorie.getDescription());
    

        if (stmt.executeUpdate() == 1) {
            return true;
        }
    } catch (SQLException e) {
        e.printStackTrace();
    }

    return false;
}
public boolean update(Categorie categorie) {
    try (Connection connection = dataSource.getConnection()) {
        PreparedStatement stmt = connection.prepareStatement( "  UPDATE article SET titre = ?, contenu = ?, auteur = ?, date = ?, image = ?, likes = ?, dislikes = ?, categorie_id = ? WHERE id = ?");
       stmt.setInt(1, categorie.getId());
       stmt.setString(2, categorie.getName());
       stmt.setString(3, categorie.getDescription());
    

        if (stmt.executeUpdate() == 1) {
            return true;
        }
    } catch (SQLException e) {
        e.printStackTrace();
    }

    return false;
}

}
