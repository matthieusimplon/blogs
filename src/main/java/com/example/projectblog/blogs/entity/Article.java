package com.example.projectblog.blogs.entity;

import java.time.LocalDate;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class Article {

    private Integer id;
    @NotBlank
    private String name;
    @NotBlank
    private String contenu;

    private LocalDate datePublication;
    @NotNull
    private String auteur;
    private String image;
    private int id_categorie;
    private int id_user;

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public Article(Integer id, @NotBlank String name, @NotBlank String contenu, LocalDate datePublication,
            @NotNull String auteur, String image, Integer id_categorie, Integer id_user) {
        this.id = id;
        this.name = name;
        this.contenu = contenu;
        this.datePublication = datePublication;
        this.auteur = auteur;
        this.image = image;
        this.id_categorie = id_categorie;
        this.id_user = id_user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public LocalDate getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(LocalDate datePublication) {
        this.datePublication = datePublication;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId_categorie() {
        return id_categorie;
    }

    public void setId_categorie(int id_categorie) {
        this.id_categorie = id_categorie;
    }

    public Article() {
    }

    public Article(@NotBlank String name, @NotBlank String contenu, LocalDate datePublication, @NotNull String auteur,
            String image, int id_categorie) {
        this.name = name;
        this.contenu = contenu;
        this.datePublication = datePublication;
        this.auteur = auteur;
        this.image = image;
        this.id_categorie = id_categorie;
    }

    public String getImageLink() {
        if (image == null) {
            return "";
        }
        if (image.startsWith("http")) {
            return image;
        }
        final String baseUrl = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
        return baseUrl + "/uploads/" + image;
    }

    public String getThumbnailLink() {
        if (image == null) {
            return "";
        }
        if (image.startsWith("http")) {
            return image;
        }
        final String baseUrl = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
        return baseUrl + "/uploads/thumbnail-" + image;
    }

}