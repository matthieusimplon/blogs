-- Active: 1717921882728@@127.0.0.1@3306@blog


DROP TABLE IF EXISTS commantaire;
DROP TABLE IF EXISTS article;
DROP TABLE IF EXISTS categorie;
DROP TABLE IF EXISTS  user;


CREATE TABLE categorie (
    id INT PRIMARY key AUTO_INCREMENT,
    nom VARCHAR(200) NOT NULL,
    description TEXT (5000)
);

CREATE TABLE user(
    id INT PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(255) NOT NULL UNIQUE, -- Pas obligatoire de mettre UNIQUE, pasque le check sera aussi fait par Spring
    password VARCHAR(255) NOT NULL,
    role VARCHAR(64) NOT NULL
);


CREATE TABLE article(
    id INT PRIMARY key AUTO_INCREMENT,
    name TEXT (500) NOT NULL,
    contenu  TEXT(5000) NOT NULL,
    datePublication DATE,
    auteur VARCHAR(50) NOT NULL,
    id_categorie INT,
    image VARCHAR(255),
    id_user  INT,
    Foreign Key (id_user) REFERENCES user(id),
    Foreign Key (id_categorie) REFERENCES categorie(id) );

CREATE TABLE commantaire(
    id INT PRIMARY KEY AUTO_INCREMENT,
    contenu  TEXT(5000),
    nonAuteur VARCHAR(100),
    dateCommantaire DATE,
    article_id INT,
    Foreign Key (article_id) REFERENCES article(id)
);


INSERT INTO user (email,password,role) VALUES 
('admin@test.com', '$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq', 'ROLE_ADMIN'),
('test@test.com', '$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq', 'ROLE_USER');







INSERT INTO categorie (id,nom,description) VALUES
(1, "sport" , "description1"),
(2, "economie", "description2"),
(3, "numérique","description3");



INSERT INTO commantaire ( contenu, nonAuteur,dateCommantaire) VALUES
("contenu1", "matthieu","2024-03-25"),
("contenu2", "lilian","2024-01-25"),
("contenu3", "maxime","2024-01-25"),
("contenu4", "fred","2024-04-25"),
("contenu5", "carole","2024-03-25"),
("contenu6", "pauline","2024-02-25"),
("contenu7", "ismaël","2024-09-25"),
("contenu8", "Amine","2024-12-25"),
("contenu9", "Océane","2024-11-25"),
("contenu10", "Romane","2024-12-25"),
("contenu11", "killian","2024-12-25"),
("contenu12", "tom","2024-11-25"),
("contenu13", "veronique","2024-10-25"),
("contenu14", "arthur","2024-07-25"),
("contenu15", "tibaut","2024-01-25");


 INSERT INTO article (name,contenu,datePublication,auteur, id_categorie, image , id_user)  VALUES
("Rugby : le club de Bobigny appelle à de « vraies actions » après des insultes racistes visant plusieurs joueuses
Lors d’une rencontre de Coupe de France à Lons (Pyrénées-Atlantiques), deux joueuses de l’équipe de Seine-Saint-Denis ont été la cible de propos à caractère raciste, dénonce le club.", "Vous pouvez partager un article en cliquant sur les icônes de partage en haut à droite de celui-ci. 
La reproduction totale ou partielle d’un article, sans l’autorisation écrite et préalable du Monde, est strictement interdite. 
Pour plus d’informations, consultez nos conditions générales de vente. 
Pour toute demande d’autorisation, contactez syndication@lemonde.fr. 
En tant qu’abonné, vous pouvez offrir jusqu’à cinq articles par mois à l’un de vos proches grâce à la fonctionnalité « Offrir un article ». 

https://www.lemonde.fr/sport/article/2024/04/03/rugby-le-club-de-bobigny-appelle-a-de-vraies-actions-apres-des-insultes-racistes-visant-plusieurs-joueuses_6225838_3242.html

Les dirigeants réclament de « vraies actions », et bien davantage « qu’un plan de communication ». Le club de rugby de Bobigny (Seine-Saint-Denis) s’est élevé, mercredi 3 avril, contre les injures racistes ayant visé deux de ses joueuses, lors d’un match dimanche à Lons, dans la périphérie de Pau (Pyrénées-Atlantiques).

« Trop c’est trop : nos joueuses professionnelles ne sont pas les seules touchées par le racisme, même nos cadettes se font insulter très régulièrement », a dénoncé Vincent Gabrelle, secrétaire général de l’AC Bobigny 93 (ACB93), dont la section féminine, les « Louves », évolue en Elite 1 (première division féminine), interrogé par l’Agence France-Presse.

Selon lui, la Fédération française de rugby (FFR), à travers sa commission anti-discrimination et égalité de traitement (Cadet), s’est « autosaisie » de la question, mardi, et une réunion de la Cadet doit avoir lieu mercredi soir, au cours de laquelle « le sujet des insultes racistes qu’ont subies nos joueuses sur et en dehors du terrain sera évoqué », a-t-il précisé.

Lire l’analyse : Article réservé à nos abonnés 2020, l’année où les footballeurs se sont élevés contre le racisme

Lors du dernier match de Coupe de France de rugby féminin, qui a vu la victoire (20-18) de Bobigny sur le club béarnais, deux joueuses ont subi des injures à caractère raciste. « L’une s’est entendue désigner par “l’Africaine aux cheveux courts” et l’autre a été traitée de “sale noire” », a expliqué M. Gabrelle, en précisant que des joueuses de l’équipe réserve Elite de Bobigny avaient déjà subi des insultes racistes deux semaines auparavant.

",'2024-03-28', "Lohan Lebreton", 1, "/src/assets/rugby.jpg", 1),
( "Fraudes à l’Assurance-maladie : 466 millions d’euros ont été détectés en 2023", "contenu2",'2024-03-28', "le monde", 2, "/src/assets/ASSURANCE_MALADIE_Logo_RVB.png" ,2),
( "Ligue des champions féminine: le carton de l’OL face à Benfica (en attendant le PSG ?)", "A l'aller, l'OL avait déjà gagné 2-1 et Cascarino avait déjà été déterminante en égalisant à 1-1 alors que les Lyonnaises étaient menées à la mi-temps. Pour cette seconde manche disputée devant près de 20.000 spectateurs, Lyon, qui affrontera en demi-finale, Paris Saint-Germain ou les Suédoises d'Häcken, opposés jeudi au Parc des Princes, a encore longtemps dominé sans marquer au cours d'une première période assez pauvre, certes sans Ada Hegerberg, blessée.Diani, un doublé dans le temps additionnel
Alors qu'Eugénie Le Sommer (13e), Delphine Cascarino (16e, 33e) ont échoué devant la cage lisboète pour de très belles opportunités, cette dernière a quand même réussi à ouvrir la marque après un service en retrait de Le Sommer, laquelle avait bénéficié d'une sortie manquée de la gardienne Lena Pauels (43e).Mais Benfica a réussi à égaliser rapidement par Marie-Yasmine Alidou à la réception d'un centre délivré par Lucia Alves (45e). Au retour des vestiaires, les remplacements de Le Sommer et Sara Däbritz par Melchie Dumornay et Damarris Egurolla ont dynamisé le jeu de l'OL et Cascarino a porté le score à 2-1 d'un tir lointain qui a surpris Pauels (51e).L'internationale, de retour depuis quelques semaines d'une rupture du ligament croisé antérieur d'un genou qui l'avait privée du Mondial 2023 l'été dernier, mais qui espère être sélectionnée pour les Jeux olympiques avec l'équipe de France, a pu laisser sa place à Vicky Becho à la 76e minute. Mais hormis le doublé de Delphine Cascarino, l'OL a de nouveau péché par son manque d'efficacité offensive malgré un doublé signé Kadidiatou Diani dans le temps additionnel (90+1e et 90+5e).",'2023-05-28', "le monde", 1, "/src/assets/BB1kEg4Q.jpg",2),
( "OpenAI a un problème : faut-il lancer Sora, son IA générative de vidéos, avant ou après les élections américaines ?", "OpenAI devrait-elle s’abstenir de lancer Sora avant les élections américaines, qui se joueront le 5 novembre 2024 ? Voilà, en somme, la question posée à l’entreprise américaine spécialisée dans l’intelligence artificielle générative, à l’occasion d’un entretien au Wall Street Journal accordé par Mira Murati, sa directrice technique, le 13 mars.

Sauf coup de théâtre, le scrutin sera un re-match entre Donald Trump et Joe Biden. Or, compte tenu de la personnalité du chef de file des Républicains, la manière dont il mène campagne, sa frustration de ne pas avoir été réélu en raison, dit-il, d’un vaste complot à son encontre, et d’un électorat de droite chauffé à blanc, des débordements sont à craindre Dans ce cocktail explosif, l’IA générative pourrait être un ingrédient de plus, susceptible de jouer un rôle néfaste dans ce rendez-vous démocratique, en trompant les électeurs et les électrices. Une problématique à laquelle OpenAI réfléchit, selon Mira Murati. Et cela pourrait bien influer sur la date de sortie de Sora.Sora est un prototype d’IA générative spécialisée dans les vidéos. Il a été présenté en février et de nombreuses vidéos de démonstration ont été mises en ligne. Le rendu est bluffant, mais soulève les mêmes questions que les autres systèmes d’IA génératives, sur le droit d’auteur et l’avenir de certains métiers créatifs.

Actuellement, les performances de l’outil lui permettent de créer des clips pouvant durer une minute avec une haute qualité d’image (1080p). Certains rendus approchent le photoréalisme et les scènes peuvent être très variées, en incluant des mouvements complexes de caméra et de mise en scène. Mais bien sûr, il y a aussi des rendus faibles ou défaillants.

Sora doit faire ses débuts publics en 2024
Au Wall Street Journal, Mira Murati a confirmé l’intention de sa société d’ouvrir Sora au public plus tard dans l’année. L’outil dévoilé en février dernier deviendrait alors librement accessible, à l’image de ses deux autres principaux produits déjà disponibles, ChatGPT (génération de texte) et Dall-E (génération d’image).Mais, a-t-elle averti, « cela pourrait prendre quelques mois. » Il y a d’abord une considération purement technique. Pas question pour OpenAI de sortir un outil manquant d’optimisation : Sora doit avoir l’empreinte électrique et de calcul la plus légère possible sur l’infrastructure de la société. Il faut que Sora puisse absorber un usage massif sans éreinter OpenAI.

Les travaux d’optimisation sont en cours, d’après Mira Murati. Surtout, il y a ensuite le timing de l’élection. Avec une campagne qui va certainement monter en puissance au fil des mois, et en raison de toutes les dérives susceptibles de se produire, dont celui de la désinformation, la sortie de Sora pourrait en souffrir.

« Nous ne publierons rien dont nous ne soyons pas sûrs de l’impact »

Mira Murati
« Vous savez, c’est assurément une considération à prendre en compte pour les questions de désinformation et de préjugés préjudiciables », a reconnu Mira Murati. En conséquence, a-t-elle poursuivi, « nous ne publierons rien dont nous ne soyons pas sûrs de l’impact sur les élections de portée globale ou sur d’autres questions ».

Tests en cours pour limiter les dégâts
De fait, il est aujourd’hui impossible d’essayer Sora. Seule une toute petite poignée de personnes triées sur le volet y a accès, dont les membres de « l’équipe rouge », chargée de pousser l’IA générative de vidéo dans ses retranchements, afin de déceler tout problème de fonctionnement. Le but est de les corriger avant l’ouverture publique.

Cette équipe rouge sert à dénicher des vulnérabilités et contribue à la mise en place de garde-fous pour ne pas générer n’importe quoi. En clair, il faut que Sora soit safe for work (SFW), c’est-à-dire qu’il maintienne une attitude convenant à toute la famille, en somme. Il faut donc traquer les biais, les fragilités et les mauvais comportements.Concernant la nudité, le péril évident est celui du deepfake pornographique. Les contenus les plus crus pourraient être interdits, mais Mira Murati relève qu’il y a aussi une réflexion sur la possibilité de conserver le droit de créer de la nudité à des fins artistiques. La société travaille avec des artistes et ce point fait partie de leurs échanges.

L’hésitation d’OpenAI rappelle celle de Midjourney. En début d’année, son patron a expliqué réfléchir à bannir la possibilité de créer des images politiques par IA — en particulier durant la campagne présidentielle américaine. Des filtres existent déjà, qui empêchent de créer des visuels imaginaires sur Trump ou Biden.

La réflexion sur la bonne fenêtre de lancement pour Sora souffre toutefois de certaines limites.

D’abord, elle occulte le fait qu’il y a déjà des fake news de nature politique qui circulent sur le net. Ensuite, l’enjeu de l’IA générative spécialisée dans la vidéo n’est pas circonscrit à Sora. Il y a et il y aura d’autres outils, plus ou moins maîtrisables. Et surtout, le défi ne sera pas réglé même après une sortie post 5 novembre. Quid des futures élections ?", "2024-03-14","Julien Lausson", 3,"/src/assets/trump-ia-1024x576.jpg" ,1);



